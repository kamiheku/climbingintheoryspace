# Climbing in Theory Space: Conquering Mountains of Truth

## Truth

*   Truth feels good or is invisible, false beliefs
    irritate.
*   Different kinds of theories: math, cryptography,
    psychopathy as adaptation.
*   The plane with the religion-science and false-true
    axes.
*   Truth is a theory that survives.
*   Best theories, for example fire, wheel or a solar
    panel, are invisible and unnoticeable.

*   My book is an evolving thing. Repository contains
    history; and fourth age repository contains more
    history.

*   Scientific induction is pre-falsification that
    cuts definitely unfruitful directions of the
    theory space away. Examples in physics include
    symmetry principles.

*   Shanzhai 42:00
*   Planned economy vs. free markets
*   Best theories don't require faith: you can test
    them everytime they come on your mind
*   Personally I find Wikipedia OK for learning new
    things.

## Who Are We?

*   Homo Sapiens is truth, a few gigabyte binary
    number, from which grows what we know as humanity
*   Human beings as pack hunters.
*   social instincts
*   emotions
*   social accounting
*   Tabula rasa does not work
*   There's no dangerous theories
*   Altruistic vessels, for example the worker
    honeybee, exist.
*   Occam's razor, which is also called minimal
    substitution principle in physics.
*   Tinder is a truth
*   Lactose tolerance
*   teknofobia ja teknointoilu
*   "IHminen on tyhmä/paha"

## Deception

*   Methods
*   Statistical deception
*   false/fake memories
*   Ennustajat

## An Alternative Strategy

*   Psychopathy
*   Institution
*   We don't believe in psychopathy because we've been taught that everybody's good

## Taming our emotions

## Modern Society

## Software
*   Copyleft
*   Evolution discovers same ticks many times, and
    humans discover same theories many times.

## Some Survivors

*   Wheel, fire, computer, Internet
*   Theory of Relativity
*   Mathematics
*   cryptography

*   are we in a simulation (religion)

## A thermal view to good and bad

*   Terminen näkökulma hyvään ja pahaan


## Table of Contents

*   Truth
*   Who Are We?
*   The Role Of Evil
*   To Control a Mind
*   Taming Emotions
*   Software
*   Our Peculiar Time
